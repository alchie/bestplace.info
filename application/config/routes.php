<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'index';
$route['404_override'] = 'index/page_not_found';
$route['translate_uri_dashes'] = TRUE;

// sitemaps
$route['sitemaps.xml'] = 'sitemap/index';
$route['sitemaps_locations.xml'] = 'sitemap/locations';
$route['sitemaps_state_(:any).xml'] = 'sitemap/location_state/$1';
$route['sitemaps_county_(:any).xml'] = 'sitemap/location_county/$1';
$route['sitemaps_city_(:any).xml'] = 'sitemap/location_city/$1';

$route['sitemaps_searches.xml'] = 'sitemap/searches';
$route['sitemap_searches_(:num).xml'] = 'sitemap/searches_items/$1';

$route['sitemap_(:num).xml'] = 'sitemap/page/$1';

$route['browse'] = 'index/browse';
$route['browse/(:num)'] = 'index/browse/$1';
$route['refer_(:num)'] = 'index/refer/$1';

#locations
$route['in/(:any)'] = 'locations/states/$1';
$route['in/(:any)/(:num)'] = 'locations/states/$1/$2';

$route['in/(:any)/(:any)'] = 'locations/counties/$1/$2';
$route['in/(:any)/(:any)/(:num)'] = 'locations/counties/$1/$2/$3';

$route['in/(:any)/(:any)/(:any)'] = 'locations/cities/$1/$2/$3';
$route['in/(:any)/(:any)/(:any)/(:num)'] = 'locations/cities/$1/$2/$3/$4';

$route['locations'] = 'locations/index';

// search
$route['search'] = 'search/index';
$route['search/(:num)'] = 'search/index/$1';
$route['search_(:any)'] = 'search/search/$1';
$route['search_(:any)/(:num)'] = 'search/search/$1/$2';
$route['search_(:any)/(:any)'] = 'search/search_category/$1/$2';
$route['search_(:any)/(:any)/(:num)'] = 'search/search_category/$1/$2/$3';
$route['search_(:any)/(:any)/(:any)'] = 'search/search_category_location/$1/$2/$3';
$route['search_(:any)/(:any)/(:any)/(:num)'] = 'search/search_category_location/$1/$2/$3/$4';

$route['categories'] = 'category/index';

// categories
$route['restaurant'] = 'category/category/restaurant';
$route['restaurant/(:num)'] = 'category/category/restaurant/$1';

$route['bar'] = 'category/category/bar';
$route['bar/(:num)'] = 'category/category/bar/$1';

$route['cafe'] = 'category/category/cafe';
$route['cafe/(:num)'] = 'category/category/cafe/$1';

$route['shopping'] = 'category/category/shopping';
$route['shopping/(:num)'] = 'category/category/shopping/$1';

$route['gas_station'] = 'category/category/gas_station';
$route['gas_station/(:num)'] = 'category/category/gas_station/$1';

#category-location
$route['(:any)/in/(:any)'] = 'category/states/$1/$2';
$route['(:any)/in/(:any)/(:num)'] = 'category/states/$1/$2/$3';

$route['(:any)/in/(:any)/(:any)'] = 'category/counties/$1/$2/$3';
$route['(:any)/in/(:any)/(:any)/(:num)'] = 'category/counties/$1/$2/$3/$4';

$route['(:any)/in/(:any)/(:any)/(:any)'] = 'category/cities/$1/$2/$3/$4';
$route['(:any)/in/(:any)/(:any)/(:any)/(:num)'] = 'category/cities/$1/$2/$3/$4/$5';

$route['(:num)/(:any)'] = 'view/by_id_slug/$1/$2';
$route['(:num)/(:any)/(:any)'] = 'view/by_id_slug/$1/$2/$3';
$route['amp/(:any)'] = 'view/amp_by_slug/$1';
$route['amp_(:any)'] = 'view/amp_by_slug/$1';
$route['(:any)'] = 'view/by_slug/$1';
$route['(:any)/(:any)'] = 'view/by_slug/$1/$2';
$route['(:any)/(:any)/(:num)'] = 'view/by_slug/$1/$2/$3';


