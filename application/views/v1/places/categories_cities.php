<?php $this->load->view('header'); ?>

<?php if( $places ) { ?>
  <div class="slider display-table center-text">
    <h1 class="title display-table-cell"><b><?php echo place_icon_name($category,true) . " in " . $city->name; ?></b></h1>
  </div><!-- slider -->
<?php } ?>

  <section class="blog-area section">
    <div class="container">

      <div class="row">
<?php
if( $places ) {
$n = 0;
foreach($places as $place) { ?>
        <div class="col-lg-4 col-md-6">
            <?php $this->load->view('places/places_card', array('place'=>$place)); ?>
        </div><!-- col-lg-4 col-md-6 -->
<?php
$n++;
  if( $n == 3) {
    $n = 0;
    echo '</div><div class="row"><!-- split -->';
  }
} 

} else {
?>

<div class="col-lg-2 col-md-0"></div>
        <div class="col-lg-8 col-md-12">
          <div class="post-wrapper">

            <h3 class="title"><b>Nothing Found!</b></h3>
            <p></p>
          

          </div><!-- post-wrapper -->
        </div><!-- col-sm-8 col-sm-offset-2 -->

<?php } ?>

 </div><!-- row -->
 
<?php if( $pagination ) { ?>
<div class="load-more-btn">
<?php echo $pagination; ?>
  </div>
<?php } ?>

     


    </div><!-- container -->
  </section><!-- section -->



<?php $this->load->view('footer'); ?>