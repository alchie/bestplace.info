<div class="card h-100">
            <div class="single-post post-style-1">

              <div class="blog-image">
<?php 
if( $place->photo_reference ) {
  $photo_url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" . $place->photo_reference . "&key=" .GOOGLE_API_KEY;
} else {
  $images = array(
    'beach-boardwalk-boat-132037.jpg',
    'beach-boat-idyllic-176400.jpg',
    'abundance-agriculture-bananas-264537.jpg',
    'basil-delicious-food-459469.jpg',
    'beach-beach-chairs-beach-hut-531035.jpg',
  );
  $image_rand = array_rand($images);
  $photo_url = base_url("assets/images/" . $images[$image_rand]);
}
?>
                <img data-src="<?php echo $photo_url; ?>" alt="Blog Image">

              </div>

              <a class="avatar" href="<?php echo site_url($place->slug); ?>"><img src="https://maps.gstatic.com/mapfiles/place_api/icons/<?php echo ($place->icon) ? $place->icon : 'generic_business'; ?>-71.png" alt="Icon Image"></a>

              <div class="blog-info">

                <h4 class="title"><a href="<?php echo site_url($place->slug); ?>"><b><?php echo $place->name; ?></b></a>
                 <br /><small><?php echo $place->vicinity; ?></small>
                 <br /><small><?php echo $place->formatted_phone_number; ?></small>
                </h4>

                <ul class="post-footer">
                  <li><a href="#rating"><i class="ion-star"></i><?php echo number_format($place->rating,1); ?></a></li>
                  <li><a href="<?php echo site_url("{$place->slug}/reviews"); ?>"><i class="ion-chatbubble"></i><?php echo $place->reviews_count; ?></a></li>
                  <li><a href="<?php echo site_url("{$place->slug}/photos"); ?>"><i class="ion-camera"></i><?php echo $place->photos_count; ?></a></li>
                </ul>

              </div><!-- blog-info -->
            </div><!-- single-post -->
          </div><!-- card -->