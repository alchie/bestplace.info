<?php $this->load->view('header'); ?>
<?php 

if( $current_place->photo_reference ) {
  $photo_url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1000&photoreference=" . $current_place->photo_reference . "&key=" .GOOGLE_API_KEY;

?>
    <div class="slider-view" style="background-image: url(<?php echo $photo_url; ?>);">
        <div class="display-table  center-text">
            <h1 class="title display-table-cell"><b><?php echo $current_place->name; ?></b></h1>
        </div>
    </div><!-- slider -->

<?php } ?>

    <section class="post-area section">
        <div class="container">

            <div class="row">

                <div class="col-lg-8 col-md-12 no-right-padding">

                    <div class="main-post">

                        <div class="blog-post-inner">
<?php if( !$current_place->photo_reference ) { ?>
<h3 class="title"><b><?php echo $current_place->name; ?></b></h3>
<?php } ?>
<?php 
    $address = ($current_place->formatted_address) ? $current_place->formatted_address : $current_place->vicinity;
    $map_address = $current_place->name . "," . $address;
?>
<iframe width="100%" height="500" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=<?php echo urlencode( $map_address ); ?>&key=<?php echo GOOGLE_API_KEY; ?>" allowfullscreen></iframe>

<?php if( $location ) { ?>
                            <ul class="tags">
                                <li><a href="<?php echo site_url("in/{$location->state_slug}/{$location->county_slug}/{$location->city_slug}"); ?>"><?php echo $location->city; ?></a></li>
                                <li><a href="<?php echo site_url("in/{$location->state_slug}/{$location->county_slug}"); ?>"><?php echo $location->county; ?></a></li>
                                <li><a href="<?php echo site_url("in/{$location->state_slug}"); ?>"><?php echo $location->state; ?></a></li>
                            </ul>

                            <ul class="tags">
                                <li><a href="<?php echo site_url("{$current_place->icon}/in/{$location->state_slug}/{$location->county_slug}/{$location->city_slug}"); ?>"><?php echo place_icon_name($current_place->icon,true) . " in " . $location->city; ?></a></li>
                                <li><a href="<?php echo site_url("{$current_place->icon}/in/{$location->state_slug}/{$location->county_slug}"); ?>"><?php echo place_icon_name($current_place->icon,true) . " in " . $location->county; ?></a></li>
                                <li><a href="<?php echo site_url("{$current_place->icon}/in/{$location->state_slug}"); ?>"><?php echo place_icon_name($current_place->icon,true) . " in " . $location->state; ?></a></li>
                            </ul>

<?php } ?>
                        </div><!-- blog-post-inner -->

                        <div class="post-icons-area">
                            <ul class="post-icons">
                                <li><a href="#"><i class="ion-star"></i><?php echo number_format($current_place->rating,1); ?></a></li>
                                <li><a href="<?php echo site_url("{$current_place->slug}/reviews"); ?>"><i class="ion-chatbubble"></i><?php echo $current_place->reviews_count; ?></a></li>
                                <li><a href="<?php echo site_url("{$current_place->slug}/photos"); ?>"><i class="ion-camera"></i><?php echo $current_place->photos_count; ?></a></li>
                            </ul>

                            <ul class="icons">
                                <li>SHARE : </li>
<?php 
$facebook_share_url = "https://www.facebook.com/dialog/share?app_id=".FACEBOOK_APP_ID."&display=popup&href=" . site_url( uri_string() ) . "&redirect_uri=" . site_url( uri_string() );
?>                                
                                <li><a target="_blank" href="<?php echo $facebook_share_url; ?>"><i class="ion-social-facebook"></i></a></li>
<!--
                                <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                                <li><a href="#"><i class="ion-social-pinterest"></i></a></li>
-->
                            </ul>
                        </div>


                    </div><!-- main-post -->
                </div><!-- col-lg-8 col-md-12 -->

                <div class="col-lg-4 col-md-12 no-left-padding">

                <?php $this->load->view('places/places_view_sidebar'); ?>

                </div><!-- col-lg-4 col-md-12 -->

            </div><!-- row -->

        </div><!-- container -->
    </section><!-- post-area -->



<?php $this->load->view('footer'); ?>