<?php if( $recommendations ) { ?>
   <section class="recomended-area section">
        <div class="container">
            <div class="row">

<?php foreach($recommendations as $recommend) { ?>
                <div class="col-lg-4 col-md-6">
                    <?php $this->load->view('places/places_card', array('place'=>$recommend)); ?>
                </div><!-- col-md-6 col-sm-12 -->
<?php } ?>

            </div><!-- row -->

        </div><!-- container -->
    </section>

<?php } ?>