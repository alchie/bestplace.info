<?php $this->load->view('header'); ?>

  <div class="slider display-table center-text">
    <h1 class="title display-table-cell"><b>Best Places in <?php echo $city->name; ?></b></h1>
  </div><!-- slider -->

  <section class="blog-area section">
    <div class="container">

      <div class="row">
<?php
$n = 0;
foreach($places as $place) { ?>
        <div class="col-lg-4 col-md-6">
            <?php $this->load->view('places/places_card', array('place'=>$place)); ?>
        </div><!-- col-lg-4 col-md-6 -->
<?php
$n++;
  if( $n == 3) {
    $n = 0;
    echo '</div><div class="row"><!-- split -->';
  }
} 
?>

 </div><!-- row -->
 
<?php if( $pagination ) { ?>
<div class="load-more-btn">
<?php echo $pagination; ?>
  </div>
<?php } ?>

     


    </div><!-- container -->
  </section><!-- section -->



<?php $this->load->view('footer'); ?>