<?php
echo '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach($locations as $location) {
	echo '<sitemap><loc>'.base_url("sitemaps_{$uri}_{$location->slug}.xml").'</loc>';
 	echo '<lastmod>'.date("Y-m-01", strtotime("-1 month")).'</lastmod></sitemap>';
}
echo '</sitemapindex>';
