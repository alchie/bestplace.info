<?php $this->load->view('header'); ?>

  <section class="blog-area section">
    <div class="container">
      <div class="row">
	      <div class="col-md-9">
	      	<?php $this->load->view('maps/states/map_'.$state->slug.'.php'); ?>
	      </div>

	      <div class="col-md-3">

				<div class="panel panel-default">
				  <div class="panel-heading">
				    <h3 class="panel-title"><?php echo $state->name; ?> Categories</h3>
				  </div>
<div class="list-group">
<?php foreach(array(
	'restaurant'=>'Restaurant',
	'bar'=>'Bar',
	'cafe'=>'Cafe',
	'shopping'=>'Shopping',
	'gas_station'=>'Gas Station',
	'lodging'=>'Hotels',
	'school'=>'Schools',
	'generic_business'=>'General Businesses',
) as $uri=>$name) { ?>
  <a class="list-group-item" href="<?php echo site_url("{$uri}/in/{$state->slug}"); ?>"><?php echo $name; ?></a>
<?php } ?>
</div>
		</div>
				</div>

	  </div>
	</div>
</section>
<?php $this->load->view('footer'); ?>