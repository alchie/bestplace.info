<?php
echo '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
$pages = ceil($total_count / $limit );
for($i=1;$i<=$pages;$i++) {
	echo "\n\t<sitemap>\n\t\t<loc>";
	echo base_url("sitemap_searches_{$i}.xml")."</loc>\n";
	echo "\t\t<lastmod>".date("Y-m-01", strtotime("-1 month"))."</lastmod>\n\t</sitemap>\n";
}
echo '</sitemapindex>';
