<?php $this->load->view('header'); ?>

  <section class="blog-area section">
    <div class="container">

      <div class="row">

<?php 
$n = 0;
foreach($places as $place) { ?>
        <div class="col-lg-4 col-md-6">
          <?php $this->load->view('places/places_card', array('place'=>$place)); ?>
        </div><!-- col-lg-4 col-md-6 -->
<?php
$n++;
  if( $n == 3) {
    $n = 0;
    echo '</div><div class="row"><!-- split -->';
  }
} 
?>

      </div><!-- row -->

      <a class="load-more-btn" href="<?php echo site_url("browse"); ?>"><b>BROWSE MORE</b></a>

    </div><!-- container -->
  </section><!-- section -->



<?php $this->load->view('footer'); ?>