<!DOCTYPE HTML>
<html lang="en">
<head>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="UTF-8">

  <title><?php echo $page_title; ?></title>
  <meta name="description" content="<?php echo $meta_description; ?>">
  <meta name="keywords" content="<?php echo $meta_keywords; ?>">
  <?php echo (isset($opengraph)) ? $opengraph : ''; ?>
  <link rel="alternate" href="<?php echo base_url(); ?>" hreflang="x-default" />
  
  <!-- Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">

  <!-- Stylesheets -->
  <link href="<?php echo base_url("assets/css/bootstrap.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/ionicons.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/styles.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/responsive.css"); ?>" rel="stylesheet">

<?php 
echo (isset($canonical_url)) ? '<link rel="canonical" href="'.$canonical_url.'" />'."\n" : ''; 
echo (isset($amphtml_url)) ? '<link rel="amphtml" href="'.$amphtml_url.'" />'."\n" : '';  
?>

</head>
<body >

<?php if(ENVIRONMENT === 'production') { ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo GOOGLE_ANALYTICS_ID; ?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '<?php echo GOOGLE_ANALYTICS_ID; ?>');
</script>

<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?php echo FACEBOOK_APP_ID; ?>',
      xfbml      : true,
      version    : 'v2.12'
    });
    FB.AppEvents.logPageView();
  };
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<?php } ?>

  <header>
    <div class="container-fluid position-relative no-side-padding">

      <a href="<?php echo site_url(); ?>" class="logo"><img src="<?php echo base_url("assets/images/logo.png"); ?>" alt="Logo Image"></a>

      <div class="menu-nav-icon" data-nav-menu="#main-menu"><i class="ion-navicon"></i></div>

<?php $this->load->view('navbar'); ?>

      <div class="src-area">
        <form method="get" action="<?php echo site_url("search"); ?>">
          <button class="src-btn" type="submit"><i class="ion-ios-search-strong"></i></button>
          <input class="src-input" type="text" placeholder="Type of search" name="q">
        </form>
      </div>

    </div><!-- conatiner -->
  </header>