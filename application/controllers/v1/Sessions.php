<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sessions extends SESSIONS_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_id = 'places';
	}

}
