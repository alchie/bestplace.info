<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends PLACES_Controller {
	
	var $list_limit = 12;

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->template_data->set('page_title', 'Map');
		$this->load->view('places/locations', $this->template_data->get_data());
	}

	public function states($slug, $start=0) {

		$states = new $this->States_model('s', 'common');
		$states->setSlug($slug,true);
		
		if( $states->nonEmpty() ) {
			$state = $states->get_results();
			$this->template_data->set('page_title', $state->name);
		
			$this->template_data->set('state', $state);
			$this->load->view('places/locations_states', $this->template_data->get_data());
		} else {
			$this->page_not_found();
		}
	}

	public function counties($state_slug, $county_slug, $start=0) {

		$states = new $this->States_model('s', 'common');
		$states->setSlug($state_slug,true);
		
		if( $states->nonEmpty() ) {
			$state = $states->get_results();
			$this->template_data->set('state', $state);

			$counties = new $this->Counties_model('s', 'common');
			$counties->setSlug($county_slug,true);

			if( $counties->nonEmpty() ) {

				$county = $counties->get_results();
				$this->template_data->set('county', $county);

				$cities = new $this->Cities_model('s', 'common');
				$cities->setCountyId($county->id,true);
				$cities->set_limit(20);
				$cities->set_start($start);
				$this->template_data->set('cities', $cities->populate());	

				$this->template_data->set('pagination', bootstrap_pagination(array(
					'uri_segment' => 4,
					'base_url' => base_url( $this->config->item('index_page') . "in/{$state->slug}/{$county->slug}" ),
					'total_rows' => $cities->count_all_results(),
					'per_page' => $cities->get_limit()
				)));

				$this->template_data->set('page_title', $county->name);		
				$this->load->view('places/locations_counties', $this->template_data->get_data());
			} else {
				$this->page_not_found();
			}

		} else {
			$this->page_not_found();
		}
	}

	public function cities($state_slug, $county_slug, $city_slug, $start=0) {

		$states = new $this->States_model('s', 'common');
		$states->setSlug($state_slug,true);
		
		if( $states->nonEmpty() ) {
			$state = $states->get_results();
			$this->template_data->set('state', $state);

			$counties = new $this->Counties_model('s', 'common');
			$counties->setSlug($county_slug,true);

			if( $counties->nonEmpty() ) {
				$county = $counties->get_results();
				$this->template_data->set('county', $county);

				$cities = new $this->Cities_model('s', 'common');
				$cities->setSlug($city_slug,true);

				if( $cities->nonEmpty() ) {
					$city = $cities->get_results();
					$this->template_data->set('city', $city);

					$page_title = "Best Places in " . $city->name;
					if( $start ) {
						$page_num = ceil($start / $this->list_limit) + 1;
						$page_title .= " - Page " . $page_num;
					}
					$this->template_data->set('page_title', $page_title);	

					$places = new $this->Places_data_model('c', 'places');
					$places->cache_on();
					$places->set_order('c.lastmod', 'DESC');
					$places->set_start( $start );
					$places->set_limit( $this->list_limit );
					$places->set_select('c.*');
					$places->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');
					$places->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
					$places->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');
					$places->setCityId($city->id,true);

					$this->template_data->set('places', $places->populate());
					$this->template_data->set('places_all', $places->count_all_results());

					$this->template_data->set('pagination', bootstrap_pagination(array(
						'uri_segment' => 5,
						'base_url' => base_url( $this->config->item('index_page') . "in/{$state->slug}/{$county->slug}/{$city->slug}" ),
						'total_rows' => $places->count_all_results(),
						'per_page' => $places->get_limit()
					)));

					$this->load->view('places/locations_cities', $this->template_data->get_data());
				} else {
					$this->page_not_found();
				}
				
			} else {
				$this->page_not_found();
			}

		} else {
			$this->page_not_found();
		}
		
	}

}
