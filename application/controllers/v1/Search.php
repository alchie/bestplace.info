<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends PLACES_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index() {		
		$this->load->library('user_agent');
		$uri = "";
		if( ( $this->input->get('q') ) && ( strlen($this->input->get('q') ) >= 3 ) && (!$this->agent->is_referral()) ) {
			$slug = slugify($this->input->get('q',true));
			$search = new $this->Places_searches_model('s', 'places');
			$search->setSlug($slug,true);
			if( $search->nonEmpty() ) {
				$result = $search->get_results();
				$times = intval( $result->times ) + 1;
				$search->setTimes( $times,false, true );
				$search->update();
			} else {
				$search->setName( trim( $this->input->get('q',true) ) );
				$search->setTimes(0);
				$search->insert();
			}

			$uri = "search_{$slug}";
			
			if( $this->input->get('category_id') || $this->input->get('location_id') ) {
				$uri .= '/';
				if( $this->input->get('category_id') ) {
					$uri .= $this->input->get('category_id');
					if( $this->input->get('location_id') ) {
						$uri .= '/';
					}
				}
				if( $this->input->get('location_id') ) {
					$uri .= $this->input->get('location_id');
				}
			}

		}
			$url = site_url($uri);
			redirect( $url );

	}

	public function search($slug, $start=0) {
		$search = new $this->Places_searches_model('s', 'places');
		$search->setSlug($slug,true);
		if( $search->nonEmpty() ) {
			$result = $search->get_results();
			$this->template_data->set('search', $result);

			$places = new $this->Places_data_model('c', 'places');
			$places->cache_on();
			$places->set_order('c.lastmod', 'DESC');
			$places->set_start( $start );
			$places->set_limit( 12 );
			$places->set_select('c.*');

		$places->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');

		$places->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
		$places->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');

			$places->set_where('name LIKE ', "%{$result->name}%");

			$this->template_data->set('places', $places->populate());
			$this->template_data->set('places_all', $places->count_all_results());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'uri_segment' => 2,
				'base_url' => base_url( $this->config->item('index_page') . "/search_" . $result->slug),
				'total_rows' => $places->count_all_results(),
				'per_page' => $places->get_limit()
			)));

			$this->load->view('places/places_search', $this->template_data->get_data());
		}
	}

}
