<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View extends PLACES_Controller {
	
	public function __construct() {
		parent::__construct();

	}

	private function _view($slug) {

		$place = new $this->Places_data_model('c', 'places');
		$place->setSlug($slug,true);
		$place->set_select("c.*");
		$place->cache_on();

		if( $place->nonEmpty() ) {
			
			$place->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');

			$place->set_select('(SELECT cr.url FROM places_redirect cr WHERE cr.place_id=c.place_id LIMIT 1) as redirect_url');

			$place->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
			$place->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');

			$place_data = $place->get();
			$this->template_data->set('current_place', $place_data);

			$this->template_data->set('location', $this->get_location_by_city($place_data->city_id) );


			$this->template_data->set('redirect_url', $place_data->redirect_url);
			$id = $place_data->place_id;

			$this->template_data->set('page_title', $place_data->name );
	    	$this->template_data->set('meta_description', "{$place_data->name} ({$place_data->formatted_phone_number}) is located at {$place_data->vicinity}.");
	    	$this->template_data->set('meta_keywords', "{$place_data->name}, {$place_data->vicinity}");

			$this->template_data->set('amphtml_url', site_url("amp_{$place_data->slug}") );

			$this->template_data->opengraph(array(
				'fb:app_id'=>$this->config->item('fbAppId'),
				'og:image' => ((isset($place_data->logo_url)) && ($place_data->logo_url)) ? $place_data->logo_url : $this->config->item('trokis_logo_url'),
				'og:description' => $this->template_data->get('meta_description'),
				'og:site_name' => 'Trokis Philippines',
				'og:title' => $place_data->name,
			));

			return true;
				
		} else {
			return false;
		}
	}

	public function by_slug($slug,$page_id='info',$item_id=NULL) {

		$this->template_data->set('page_id', $page_id);

		if( $this->_view($slug) ) {

			$place_data = $this->template_data->get('current_place');

			switch($page_id) {
				case 'map':
					$this->template_data->set('page_title', $place_data->name . " - Map Location" );
					$this->load->view('places/places_view_map', $this->template_data->get_data());
				break;
				case 'contact':
					$this->_must_login('login', uri_string());
					$this->_contact_action($place_data);
					$this->template_data->set('page_title', $place_data->name . " - Contact Form" );
					$this->load->view('places/places_view_contact', $this->template_data->get_data());
				break;
				case 'gallery':
					$this->_media_actions($place_data, 'image', array('gallery', '360'));
					$this->template_data->set('page_title', $place_data->name . " - Gallery" );
					$this->load->view('places/places_view_gallery', $this->template_data->get_data());
				break;
				case 'image':
					$this->template_data->set('page_id', 'gallery');
					$this->_media_actions($place_data, 'image', array('gallery', '360'));
					$this->_media_data($item_id);
					$this->template_data->set('page_title', $place_data->name . " - Image Preview" );
					$this->load->view('places/places_view_gallery_image', $this->template_data->get_data());
				break;
				case 'videos':
					$this->_media_actions($place_data, 'video', 'youtube');
					$this->template_data->set('page_title', $place_data->name . " - Videos" );
					$this->load->view('places/places_view_videos', $this->template_data->get_data());
				break;
				case 'video':
					$this->template_data->set('page_id', 'videos');
					$this->_media_actions($place_data, 'video', 'youtube');
					$this->_media_data($item_id);
					$this->template_data->set('page_title', $place_data->name . " - Image Preview" );
					$this->load->view('places/places_view_videos_video', $this->template_data->get_data());
				break;
				case 'products':
					$this->template_data->set('page_title', $place_data->name . " - Products" );
					$this->load->view('places/places_view_products', $this->template_data->get_data());
				break;
				case 'services':
					$this->template_data->set('page_title', $place_data->name . " - Services" );
					$this->load->view('places/places_view_services', $this->template_data->get_data());
				break;
				case 'claims':

					$claims = new $this->Places_claimed_model('cc', 'places');
		            $claims->setPlaceId($place_data->id,true);
		            $claims->set_join('users u', 'u.uid=cc.user_id', '', $this->db_configs['account']['database']);
		            $claims->set_select("cc.*");
		            $claims->set_select("u.full_name");
		            $this->template_data->set('claims', $claims->populate());

					$this->template_data->set('page_title', $place_data->name . " - Claims" );
					$this->load->view('places/places_view_claims', $this->template_data->get_data());
				break;
				case 'info':
				default:
					$this->load->view('places/places_view', $this->template_data->get_data());
				break;
			}
			
		} else {
			$this->load->view('page_not_found', $this->template_data->get_data());
		}
	}

	public function amp_by_slug($slug) {
		if( $this->_view($slug) ) {
			$this->load->view('places/places_amp', $this->template_data->get_data());
		} else {
			$this->load->view('page_not_found', $this->template_data->get_data());
		}
	}


	private function _media_actions($place_data, $mtype='image', $mkey='gallery') {
		$submitted = new $this->Places_media_model('cm', 'places');
		$submitted->setPlaceId($place_data->id,true);
		$submitted->setMediaType($mtype,true);
		//$submitted->setMediaKey($mkey,true);
		$submitted->set_where_in('cm.media_key',$mkey);
		$submitted->setActive(1,true);
		$submitted->cache_on();
		$this->template_data->set('images', $submitted->populate());

		if( $this->session->userdata('loggedIn') ) {
		if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) {

			$unapproved = new $this->Places_media_model('cm', 'places');
			$unapproved->setPlaceId($place_data->id,true);
			$unapproved->setMediaType($mtype,true);
			//$unapproved->setMediaKey($mkey,true);
			$unapproved->set_where_in('cm.media_key',$mkey);
			$unapproved->setActive(0,true);
			$this->template_data->set('unapproved', $unapproved->populate());

		}}


	}

	private function _media_data($media_id) {
		$current_media = new $this->Places_media_model(NULL, 'places');
		$current_media->setId($media_id,true);

		if( $this->session->userdata('loggedIn') ) {
		if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) {
			if( $this->input->get('approve') ) {
				$current_media->setActive(1,false,true);
				$current_media->update();
				redirect( site_url($this->input->get('next')) );
			}
			if( $this->input->get('delete') ) {
				$current_media->delete();
				redirect( site_url($this->input->get('next')) );
			}
		} else {
			$current_media->cache_on();
		}}

		$this->template_data->set('current_media', $current_media->get());
	}

	private function _contact_action($place_data) {
		if( $this->input->post() ) {
		$to = "company{$place_data->id}@trokis.com";
		$full_name = strip_tags($this->input->post("full_name",true));
		$phone_number = strip_tags($this->input->post("phone_number",true));
		$email_address = strip_tags($this->input->post("email_address",true));
		$message = strip_tags($this->input->post("message",true));
		$message_body = "FULL NAME: {$full_name}\n";
		$message_body .= "PHONE NUMBER: {$phone_number}\n";
		$message_body .= "EMAIL ADDRESS: {$email_address}\n";
		$message_body .= "- - - - - - - - - - - - - - - - - - - - - \n\n";
		$message_body .= "{$message}\n\n";
		$message_body .= "SOURCE: " . site_url($place_data->slug);
		$message_body = urlencode($message_body);
		$gmail_link = "https://mail.google.com/mail/?view=cm&fs=1&to={$to}&su=TROKIS.COM&body={$message_body}";
		redirect( $gmail_link );

		}

	}

}
