<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends PLACES_Controller {

	public function __construct() {
		
		parent::__construct();

	}

	public function index()
	{
		$this->session->sess_destroy();
        redirect( account_url("logout") . "?app=" . $this->template_data->get('app_id') );	
	}

}
