<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Categories_model Class
 *
 * Manipulates `categories` table on database

CREATE TABLE `categories` (
  `category_id` int(20) NOT NULL AUTO_INCREMENT,
  `term_id` int(20) NOT NULL,
  `parent_category` int(20) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `priority` int(5) NOT NULL DEFAULT '0',
  `subdomain` int(1) NOT NULL DEFAULT '0',
  `main_menu` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`)
);

ALTER TABLE  `categories` ADD  `category_id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `categories` ADD  `term_id` int(20) NOT NULL   ;
ALTER TABLE  `categories` ADD  `parent_category` int(20) NOT NULL   DEFAULT '0';
ALTER TABLE  `categories` ADD  `active` int(1) NOT NULL   DEFAULT '1';
ALTER TABLE  `categories` ADD  `priority` int(5) NOT NULL   DEFAULT '0';
ALTER TABLE  `categories` ADD  `subdomain` int(1) NOT NULL   DEFAULT '0';
ALTER TABLE  `categories` ADD  `main_menu` int(1) NOT NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Categories_model extends MY_Model {

	protected $category_id;
	protected $term_id;
	protected $parent_category;
	protected $active;
	protected $priority;
	protected $subdomain;
	protected $main_menu;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'categories';
		$this->_short_name = 'categories';
		$this->_fields = array("category_id","term_id","parent_category","active","priority","subdomain","main_menu");
		$this->_required = array("term_id","parent_category","active","priority","subdomain","main_menu");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: category_id -------------------------------------- 

	/** 
	* Sets a value to `category_id` variable
	* @access public
	*/

	public function setCategoryId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('category_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `category_id` variable
	* @access public
	*/

	public function getCategoryId() {
		return $this->category_id;
	}
	
// ------------------------------ End Field: category_id --------------------------------------


// ---------------------------- Start Field: term_id -------------------------------------- 

	/** 
	* Sets a value to `term_id` variable
	* @access public
	*/

	public function setTermId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('term_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `term_id` variable
	* @access public
	*/

	public function getTermId() {
		return $this->term_id;
	}
	
// ------------------------------ End Field: term_id --------------------------------------


// ---------------------------- Start Field: parent_category -------------------------------------- 

	/** 
	* Sets a value to `parent_category` variable
	* @access public
	*/

	public function setParentCategory($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('parent_category', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `parent_category` variable
	* @access public
	*/

	public function getParentCategory() {
		return $this->parent_category;
	}
	
// ------------------------------ End Field: parent_category --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

	public function getActive() {
		return $this->active;
	}
	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: priority -------------------------------------- 

	/** 
	* Sets a value to `priority` variable
	* @access public
	*/

	public function setPriority($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('priority', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `priority` variable
	* @access public
	*/

	public function getPriority() {
		return $this->priority;
	}
	
// ------------------------------ End Field: priority --------------------------------------


// ---------------------------- Start Field: subdomain -------------------------------------- 

	/** 
	* Sets a value to `subdomain` variable
	* @access public
	*/

	public function setSubdomain($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('subdomain', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `subdomain` variable
	* @access public
	*/

	public function getSubdomain() {
		return $this->subdomain;
	}
	
// ------------------------------ End Field: subdomain --------------------------------------


// ---------------------------- Start Field: main_menu -------------------------------------- 

	/** 
	* Sets a value to `main_menu` variable
	* @access public
	*/

	public function setMainMenu($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('main_menu', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `main_menu` variable
	* @access public
	*/

	public function getMainMenu() {
		return $this->main_menu;
	}
	
// ------------------------------ End Field: main_menu --------------------------------------



	
	public function get_table_options() {
		return array(
			'category_id' => (object) array(
										'Field'=>'category_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'term_id' => (object) array(
										'Field'=>'term_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'parent_category' => (object) array(
										'Field'=>'parent_category',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'active' => (object) array(
										'Field'=>'active',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									),

			'priority' => (object) array(
										'Field'=>'priority',
										'Type'=>'int(5)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'subdomain' => (object) array(
										'Field'=>'subdomain',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'main_menu' => (object) array(
										'Field'=>'main_menu',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'category_id' => "ALTER TABLE  `categories` ADD  `category_id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'term_id' => "ALTER TABLE  `categories` ADD  `term_id` int(20) NOT NULL   ;",
			'parent_category' => "ALTER TABLE  `categories` ADD  `parent_category` int(20) NOT NULL   DEFAULT '0';",
			'active' => "ALTER TABLE  `categories` ADD  `active` int(1) NOT NULL   DEFAULT '1';",
			'priority' => "ALTER TABLE  `categories` ADD  `priority` int(5) NOT NULL   DEFAULT '0';",
			'subdomain' => "ALTER TABLE  `categories` ADD  `subdomain` int(1) NOT NULL   DEFAULT '0';",
			'main_menu' => "ALTER TABLE  `categories` ADD  `main_menu` int(1) NOT NULL   DEFAULT '0';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Categories_model.php */
/* Location: ./application/models/Categories_model.php */
