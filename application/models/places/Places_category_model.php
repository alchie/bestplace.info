<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Places_category_model Class
 *
 * Manipulates `places_category` table on database

CREATE TABLE `places_category` (
  `place_id` int(20) NOT NULL,
  `category_id` int(20) NOT NULL,
  KEY `place_id` (`place_id`)
);

ALTER TABLE  `places_category` ADD  `place_id` int(20) NOT NULL   ;
ALTER TABLE  `places_category` ADD  `category_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Places_category_model extends MY_Model {

	protected $place_id;
	protected $category_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'places_category';
		$this->_short_name = 'places_category';
		$this->_fields = array("place_id","category_id");
		$this->_required = array("place_id","category_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: place_id -------------------------------------- 

	/** 
	* Sets a value to `place_id` variable
	* @access public
	*/

	public function setPlaceId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('place_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `place_id` variable
	* @access public
	*/

	public function getPlaceId() {
		return $this->place_id;
	}
	
// ------------------------------ End Field: place_id --------------------------------------


// ---------------------------- Start Field: category_id -------------------------------------- 

	/** 
	* Sets a value to `category_id` variable
	* @access public
	*/

	public function setCategoryId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('category_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `category_id` variable
	* @access public
	*/

	public function getCategoryId() {
		return $this->category_id;
	}
	
// ------------------------------ End Field: category_id --------------------------------------



	
	public function get_table_options() {
		return array(
			'place_id' => (object) array(
										'Field'=>'place_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'category_id' => (object) array(
										'Field'=>'category_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'place_id' => "ALTER TABLE  `places_category` ADD  `place_id` int(20) NOT NULL   ;",
			'category_id' => "ALTER TABLE  `places_category` ADD  `category_id` int(20) NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Places_category_model.php */
/* Location: ./application/models/Places_category_model.php */
