<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Places_meta_model Class
 *
 * Manipulates `places_meta` table on database

CREATE TABLE `places_meta` (
  `place_id` int(20) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` text NOT NULL,
  `active` int(1) DEFAULT '1',
  KEY `place_id` (`place_id`)
);

ALTER TABLE  `places_meta` ADD  `place_id` int(20) NOT NULL   ;
ALTER TABLE  `places_meta` ADD  `meta_key` varchar(200) NOT NULL   ;
ALTER TABLE  `places_meta` ADD  `meta_value` text NOT NULL   ;
ALTER TABLE  `places_meta` ADD  `active` int(1) NULL   DEFAULT '1';


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Places_meta_model extends MY_Model {

	protected $place_id;
	protected $meta_key;
	protected $meta_value;
	protected $active;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'places_meta';
		$this->_short_name = 'places_meta';
		$this->_fields = array("place_id","meta_key","meta_value","active");
		$this->_required = array("place_id","meta_key","meta_value");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: place_id -------------------------------------- 

	/** 
	* Sets a value to `place_id` variable
	* @access public
	*/

	public function setPlaceId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('place_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `place_id` variable
	* @access public
	*/

	public function getPlaceId() {
		return $this->place_id;
	}
	
// ------------------------------ End Field: place_id --------------------------------------


// ---------------------------- Start Field: meta_key -------------------------------------- 

	/** 
	* Sets a value to `meta_key` variable
	* @access public
	*/

	public function setMetaKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('meta_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `meta_key` variable
	* @access public
	*/

	public function getMetaKey() {
		return $this->meta_key;
	}
	
// ------------------------------ End Field: meta_key --------------------------------------


// ---------------------------- Start Field: meta_value -------------------------------------- 

	/** 
	* Sets a value to `meta_value` variable
	* @access public
	*/

	public function setMetaValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('meta_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `meta_value` variable
	* @access public
	*/

	public function getMetaValue() {
		return $this->meta_value;
	}
	
// ------------------------------ End Field: meta_value --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

	public function getActive() {
		return $this->active;
	}
	
// ------------------------------ End Field: active --------------------------------------



	
	public function get_table_options() {
		return array(
			'place_id' => (object) array(
										'Field'=>'place_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'meta_key' => (object) array(
										'Field'=>'meta_key',
										'Type'=>'varchar(200)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'meta_value' => (object) array(
										'Field'=>'meta_value',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'active' => (object) array(
										'Field'=>'active',
										'Type'=>'int(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'place_id' => "ALTER TABLE  `places_meta` ADD  `place_id` int(20) NOT NULL   ;",
			'meta_key' => "ALTER TABLE  `places_meta` ADD  `meta_key` varchar(200) NOT NULL   ;",
			'meta_value' => "ALTER TABLE  `places_meta` ADD  `meta_value` text NOT NULL   ;",
			'active' => "ALTER TABLE  `places_meta` ADD  `active` int(1) NULL   DEFAULT '1';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Places_meta_model.php */
/* Location: ./application/models/Places_meta_model.php */
