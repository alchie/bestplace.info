-- Table structure for table `categories` 

CREATE TABLE `categories` (
  `category_id` int(20) NOT NULL AUTO_INCREMENT,
  `term_id` int(20) NOT NULL,
  `parent_category` int(20) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `priority` int(5) NOT NULL DEFAULT '0',
  `subdomain` int(1) NOT NULL DEFAULT '0',
  `main_menu` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`)
);

-- Table structure for table `places_category` 

CREATE TABLE `places_category` (
  `place_id` int(20) NOT NULL,
  `category_id` int(20) NOT NULL,
  KEY `place_id` (`place_id`)
);

-- Table structure for table `places_claimed` 

CREATE TABLE `places_claimed` (
  `place_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `icon` text,
  `rating` decimal(10,5) DEFAULT NULL,
  `url` text,
  `vicinity` text,
  `website` text,
  `international_phone_number` text,
  `formatted_phone_number` text,
  `formatted_address` text,
  `geo_loc_lat` decimal(10,8) DEFAULT NULL,
  `geo_loc_lng` decimal(11,8) DEFAULT NULL,
  `lastmod` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `owner_id` int(20) DEFAULT NULL,
  `user_id` int(20) NOT NULL
);

-- Table structure for table `places_data` 

CREATE TABLE `places_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `place_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `icon` text,
  `rating` decimal(10,5) DEFAULT NULL,
  `url` text,
  `vicinity` text,
  `website` text,
  `international_phone_number` text,
  `formatted_phone_number` text,
  `formatted_address` text,
  `geo_loc_lat` decimal(10,8) DEFAULT NULL,
  `geo_loc_lng` decimal(11,8) DEFAULT NULL,
  `city_id` int(20) DEFAULT NULL,
  `lastmod` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `owner_id` int(20) DEFAULT NULL,
  `done` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`place_id`),
  UNIQUE KEY `slug` (`slug`),
  UNIQUE KEY `place_id` (`place_id`),
  UNIQUE KEY `id` (`id`)
);

-- Table structure for table `places_keyword` 

CREATE TABLE `places_keyword` (
  `place_id` int(20) NOT NULL,
  `keyword_id` int(20) NOT NULL,
  KEY `place_id` (`place_id`)
);

-- Table structure for table `places_location` 

CREATE TABLE `places_location` (
  `place_id` int(20) NOT NULL,
  `location_id` int(20) NOT NULL
);

-- Table structure for table `places_media` 

CREATE TABLE `places_media` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `place_id` varchar(255) NOT NULL,
  `media_type` varchar(100) NOT NULL,
  `media_key` varchar(200) NOT NULL,
  `media_value` text,
  `media_title` varchar(200) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `claim_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `place_id` (`place_id`)
);

-- Table structure for table `places_meta` 

CREATE TABLE `places_meta` (
  `place_id` int(20) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` text NOT NULL,
  `active` int(1) DEFAULT '1',
  KEY `place_id` (`place_id`)
);

-- Table structure for table `places_photos` 

CREATE TABLE `places_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` varchar(100) NOT NULL,
  `photo_reference` text NOT NULL,
  `height` int(10) NOT NULL,
  `width` int(10) NOT NULL,
  `contributor` varchar(100) NOT NULL,
  `contrib_url` text NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `places_redirect` 

CREATE TABLE `places_redirect` (
  `place_id` int(20) NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`place_id`)
);

-- Table structure for table `places_reviews` 

CREATE TABLE `places_reviews` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `place_id` varchar(100) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `author_id` varchar(100) NOT NULL,
  `profile_photo_url` text NOT NULL,
  `rating` int(10) NOT NULL,
  `text` text NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `places_searches` 

CREATE TABLE `places_searches` (
  `slug` varchar(150) NOT NULL,
  `name` varchar(100) NOT NULL,
  `times` int(10) NOT NULL DEFAULT '0',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`slug`)
);

-- Table structure for table `places_type` 

CREATE TABLE `places_type` (
  `place_id` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL
);

