-- Table structure for table `app_sessions` 

CREATE TABLE `app_sessions` (
  `app_id` varchar(20) NOT NULL,
  `session_id` text NOT NULL,
  `user_id` int(20) NOT NULL
);

-- Table structure for table `ci_sessions` 

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
);

-- Table structure for table `user_sessions` 

CREATE TABLE `user_sessions` (
  `user_id` varchar(200) NOT NULL,
  `session_id` varchar(200) NOT NULL
);

