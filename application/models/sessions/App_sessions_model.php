<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * App_sessions_model Class
 *
 * Manipulates `app_sessions` table on database

CREATE TABLE `app_sessions` (
  `app_id` varchar(20) NOT NULL,
  `session_id` text NOT NULL,
  `user_id` int(20) NOT NULL
);

 * @package			        Model
 * @version_number	        3.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class App_sessions_model extends MY_Model {

	protected $app_id;
	protected $session_id;
	protected $user_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'app_sessions';
		$this->_short_name = 'app_sessions';
		$this->_fields = array("app_id","session_id","user_id");
		$this->_required = array("app_id","session_id","user_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: app_id -------------------------------------- 

	/** 
	* Sets a value to `app_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setAppId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('app_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `app_id` variable
	* @access public
	* @return String;
	*/

		public function getAppId() {
			return $this->app_id;
		}
	
// ------------------------------ End Field: app_id --------------------------------------


// ---------------------------- Start Field: session_id -------------------------------------- 

	/** 
	* Sets a value to `session_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setSessionId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('session_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `session_id` variable
	* @access public
	* @return String;
	*/

		public function getSessionId() {
			return $this->session_id;
		}
	
// ------------------------------ End Field: session_id --------------------------------------


// ---------------------------- Start Field: user_id -------------------------------------- 

	/** 
	* Sets a value to `user_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setUserId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('user_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `user_id` variable
	* @access public
	* @return String;
	*/

		public function getUserId() {
			return $this->user_id;
		}
	
// ------------------------------ End Field: user_id --------------------------------------




}

/* End of file App_sessions_model.php */
/* Location: ./application/models/App_sessions_model.php */
