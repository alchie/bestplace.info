<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User_sessions_model Class
 *
 * Manipulates `user_sessions` table on database

CREATE TABLE `user_sessions` (
  `user_id` varchar(200) NOT NULL,
  `session_id` varchar(200) NOT NULL
);

 * @package			        Model
 * @version_number	        3.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class User_sessions_model extends MY_Model {

	protected $user_id;
	protected $session_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'user_sessions';
		$this->_short_name = 'user_sessions';
		$this->_fields = array("user_id","session_id");
		$this->_required = array("user_id","session_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: user_id -------------------------------------- 

	/** 
	* Sets a value to `user_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setUserId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('user_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `user_id` variable
	* @access public
	* @return String;
	*/

		public function getUserId() {
			return $this->user_id;
		}
	
// ------------------------------ End Field: user_id --------------------------------------


// ---------------------------- Start Field: session_id -------------------------------------- 

	/** 
	* Sets a value to `session_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setSessionId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('session_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `session_id` variable
	* @access public
	* @return String;
	*/

		public function getSessionId() {
			return $this->session_id;
		}
	
// ------------------------------ End Field: session_id --------------------------------------




}

/* End of file User_sessions_model.php */
/* Location: ./application/models/User_sessions_model.php */
