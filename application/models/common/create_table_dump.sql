-- Table structure for table `cities` 

CREATE TABLE `cities` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `county_id` int(20) NOT NULL,
  `state_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `counties` 

CREATE TABLE `counties` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `state_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `states` 

CREATE TABLE `states` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `slug` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
);

-- Table structure for table `terms` 

CREATE TABLE `terms` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

