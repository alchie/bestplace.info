<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Recovery_codes_model Class
 *
 * Manipulates `recovery_codes` table on database

CREATE TABLE `recovery_codes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `code` varchar(200) NOT NULL,
  `expiry` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

ALTER TABLE  `recovery_codes` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `recovery_codes` ADD  `user_id` int(20) NOT NULL   ;
ALTER TABLE  `recovery_codes` ADD  `code` varchar(200) NOT NULL   ;
ALTER TABLE  `recovery_codes` ADD  `expiry` datetime NOT NULL   DEFAULT 'CURRENT_TIMESTAMP';


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Recovery_codes_model extends MY_Model {

	protected $id;
	protected $user_id;
	protected $code;
	protected $expiry;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'recovery_codes';
		$this->_short_name = 'recovery_codes';
		$this->_fields = array("id","user_id","code","expiry");
		$this->_required = array("user_id","code","expiry");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: user_id -------------------------------------- 

	/** 
	* Sets a value to `user_id` variable
	* @access public
	*/

	public function setUserId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('user_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `user_id` variable
	* @access public
	*/

	public function getUserId() {
		return $this->user_id;
	}
	
// ------------------------------ End Field: user_id --------------------------------------


// ---------------------------- Start Field: code -------------------------------------- 

	/** 
	* Sets a value to `code` variable
	* @access public
	*/

	public function setCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('code', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `code` variable
	* @access public
	*/

	public function getCode() {
		return $this->code;
	}
	
// ------------------------------ End Field: code --------------------------------------


// ---------------------------- Start Field: expiry -------------------------------------- 

	/** 
	* Sets a value to `expiry` variable
	* @access public
	*/

	public function setExpiry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('expiry', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `expiry` variable
	* @access public
	*/

	public function getExpiry() {
		return $this->expiry;
	}
	
// ------------------------------ End Field: expiry --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'user_id' => (object) array(
										'Field'=>'user_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'code' => (object) array(
										'Field'=>'code',
										'Type'=>'varchar(200)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'expiry' => (object) array(
										'Field'=>'expiry',
										'Type'=>'datetime',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'CURRENT_TIMESTAMP',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `recovery_codes` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'user_id' => "ALTER TABLE  `recovery_codes` ADD  `user_id` int(20) NOT NULL   ;",
			'code' => "ALTER TABLE  `recovery_codes` ADD  `code` varchar(200) NOT NULL   ;",
			'expiry' => "ALTER TABLE  `recovery_codes` ADD  `expiry` datetime NOT NULL   DEFAULT 'CURRENT_TIMESTAMP';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Recovery_codes_model.php */
/* Location: ./application/models/Recovery_codes_model.php */
