<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if( ! function_exists('trokis_cookie') ) {
	function trokis_cookie($arg1, $arg2=NULL) {
        $CI = get_instance();
        $cookie_domain = $CI->config->item('cookie_domain');
        $cookie_path = $CI->config->item('cookie_path');

        $vars = "";
        if( gettype($arg1) == 'array' ) {
            $arg1_r = array();
            foreach($arg1 as $arg1_k=>$arg1_v) {
                $arg1_r[] = "{$arg1_k}={$arg1_v}";
            }
            $vars = implode(",", $arg1_r);
        } else {
            $vars = "{$arg1}={$arg2}";
        }
        $CI->output->set_header("Set-Cookie: {$vars}; path={$cookie_path}; domain={$cookie_domain};");
	}
}

if( ! function_exists('current_location_cookie') ) {
    function current_location_cookie($id,$r=NULL) {
        $CI = get_instance();
        $cookie_domain = $CI->config->item('cookie_domain');
        $cookie_path = $CI->config->item('cookie_path');
        $CI->output->set_header("Set-Cookie: current_location={$id}; path={$cookie_path}; domain={$cookie_domain};");
        if( $r ) {
            redirect($r);
        }
    }
}

if( ! function_exists('set_referral_id') ) {
    function set_referral_id($id,$r=NULL) {
        $CI = get_instance();
        $cookie_domain = $CI->config->item('cookie_domain');
        $cookie_path = $CI->config->item('cookie_path');
        $CI->output->set_header("Set-Cookie: referral_id={$id}; path={$cookie_path}; domain={$cookie_domain};");
        if( $r ) {
            redirect($r);
        } 
    }
}

if( ! function_exists('schema_breadcrumbs') ) {
    function schema_breadcrumbs($items) {
        $n = 1;
        echo '<ol class="breadcrumb hidden-xs" itemscope itemtype="http://schema.org/BreadcrumbList">';
        foreach($items as $item) {
            $active_class = ($n==count($items)) ? "class=\"active\"" : "";
            $link_tag = ($n==count($items)) ? "span" : "a";
            echo "\n<li itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\" $active_class><{$link_tag} itemprop=\"item\" href=\"{$item['url']}\"><span itemprop=\"name\">{$item['title']}</span></{$link_tag}><meta itemprop=\"title\" content=\"{$item['title']}\" /></li>";
            $n++;
        }
        echo "\n</ol>";
    }
}

if( ! function_exists('magazine_breadcrumbs') ) {
    function magazine_breadcrumbs($items) {
        $n = 1;
        echo '<nav class="mh-breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';
        foreach($items as $item) {
            $active_class = ($n==count($items)) ? "class=\"active\"" : "";
            $link_tag = ($n==count($items)) ? "span" : "a";
            $link_attr = ($n==count($items)) ? "content" : "href";
            echo "\n\t\t<span itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\" $active_class>\n\t\t\t<{$link_tag} itemprop=\"item\" {$link_attr}=\"{$item['url']}\">\n\t\t\t<span itemprop=\"name\">{$item['title']}</span></{$link_tag}>\n\t\t\t<meta itemprop=\"title\" content=\"{$item['title']}\" />\n\t\t\t<meta itemprop=\"position\" content=\"{$n}\" />\n\t\t</span>";
            if( $n != count($items) ) {
              echo "\n\n\t\t".'<span class="mh-breadcrumb-delimiter"><i class="fa fa-angle-right"></i></span>'."\n";
            }
            $n++;
        }
        echo "\n</nav>";
    }
}

if( ! function_exists('slugify') ) {
    function slugify($text)
    {
      // replace non letter or digits by -
      $text = preg_replace('~[^\pL\d]+~u', '-', $text);

      // transliterate
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

      // remove unwanted characters
      $text = preg_replace('~[^-\w]+~', '', $text);

      // trim
      $text = trim($text, '-');

      // remove duplicate -
      $text = preg_replace('~-+~', '-', $text);

      // lowercase
      $text = strtolower($text);

      if (empty($text)) {
        return 'n-a';
      }

      return $text;
    }
}

if( ! function_exists('place_icon_name') ) {
    function place_icon_name($slug, $plural=false)
    {
      $icons = array();
      if( $plural ) {
            $icons['airport'] = 'Airports';
            $icons['amusement'] = 'Amusements';
            $icons['aquarium'] = 'Aquariums';
            $icons['art_gallery'] = 'Art Galleries';
            $icons['atm'] = 'Atms';
            $icons['baby'] = 'Babies';
            $icons['bank_dollar'] = 'Bank Dollar';
            $icons['bank_euro'] = 'Bank Euro';
            $icons['bank_pound'] = 'Bank Pound';
            $icons['bank_yen'] = 'Bank Yen';
            $icons['barber'] = 'Barbers';
            $icons['bar'] = 'Bars';
            $icons['baseball'] = 'Baseballs';
            $icons['beach'] = 'Beaches';
            $icons['bicycle'] = 'Bicycles';
            $icons['bowling'] = 'Bowlings';
            $icons['bus'] = 'Buses';
            $icons['cafe'] = 'Cafes';
            $icons['camping'] = 'Campings';
            $icons['car_dealer'] = 'Car Dealers';
            $icons['car_rental'] = 'Car Rentals';
            $icons['car_repair'] = 'Car Repairs';
            $icons['casino'] = 'Casinos';
            $icons['civic_building'] = 'Civic Buildings';
            $icons['convenience'] = 'Conveniences';
            $icons['courthouse'] = 'Courthouses';
            $icons['dentist'] = 'Dentists';
            $icons['doctor'] = 'Doctors';
            $icons['electronics'] = 'Electronics';
            $icons['fitness'] = 'Fitness';
            $icons['flower'] = 'Flowers';
            $icons['gas_station'] = 'Gas Stations';
            $icons['generic_business'] = 'Generic Businesses';
            $icons['generic_recreational'] = 'Generic Recreationals';
            $icons['geocode'] = 'Geocode';
            $icons['golf'] = 'Golfs';
            $icons['government'] = 'Governments';
            $icons['historic'] = 'Historics';
            $icons['jewelry'] = 'Jewelries';
            $icons['library'] = 'Libraries';
            $icons['lodging'] = 'Lodgings';
            $icons['monument'] = 'Monuments';
            $icons['mountain'] = 'Mountains';
            $icons['movies'] = 'Movies';
            $icons['museum'] = 'Museums';
            $icons['pet'] = 'Pets';
            $icons['police'] = 'Polices';
            $icons['post_office'] = 'Post Offices';
            $icons['repair'] = 'Repairs';
            $icons['restaurant'] = 'Restaurants';
            $icons['school'] = 'Schools';
            $icons['shopping'] = 'Shoppings';
            $icons['ski'] = 'Skis';
            $icons['stadium'] = 'Stadiums';
            $icons['supermarket'] = 'Supermarkets';
            $icons['taxi'] = 'Taxis';
            $icons['tennis'] = 'Tennis';
            $icons['train'] = 'Trains';
            $icons['travel_agent'] = 'Travel Agents';
            $icons['truck'] = 'Trucks';
            $icons['university'] = 'Universities';
            $icons['wine'] = 'Wines';
            $icons['worship_christian'] = 'Worship Christian';
            $icons['worship_general'] = 'Worship General';
            $icons['worship_hindu'] = 'Worship Hindu';
            $icons['worship_islam'] = 'Worship Islam';
            $icons['worship_jewish'] = 'Worship Jewish';
            $icons['zoo'] = 'Zoo';
      } else {
          $icons['airport'] = 'Airport';
          $icons['amusement'] = 'Amusement';
          $icons['aquarium'] = 'Aquarium';
          $icons['art_gallery'] = 'Art Gallery';
          $icons['atm'] = 'Atm';
          $icons['baby'] = 'Baby';
          $icons['bank_dollar'] = 'Bank Dollar';
          $icons['bank_euro'] = 'Bank Euro';
          $icons['bank_pound'] = 'Bank Pound';
          $icons['bank_yen'] = 'Bank Yen';
          $icons['barber'] = 'Barber';
          $icons['bar'] = 'Bar';
          $icons['baseball'] = 'Baseball';
          $icons['beach'] = 'Beach';
          $icons['bicycle'] = 'Bicycle';
          $icons['bowling'] = 'Bowling';
          $icons['bus'] = 'Bus';
          $icons['cafe'] = 'Cafe';
          $icons['camping'] = 'Camping';
          $icons['car_dealer'] = 'Car Dealer';
          $icons['car_rental'] = 'Car Rental';
          $icons['car_repair'] = 'Car Repair';
          $icons['casino'] = 'Casino';
          $icons['civic_building'] = 'Civic Building';
          $icons['convenience'] = 'Convenience';
          $icons['courthouse'] = 'Courthouse';
          $icons['dentist'] = 'Dentist';
          $icons['doctor'] = 'Doctor';
          $icons['electronics'] = 'Electronics';
          $icons['fitness'] = 'Fitness';
          $icons['flower'] = 'Flower';
          $icons['gas_station'] = 'Gas Station';
          $icons['generic_business'] = 'Generic Business';
          $icons['generic_recreational'] = 'Generic Recreational';
          $icons['geocode'] = 'Geocode';
          $icons['golf'] = 'Golf';
          $icons['government'] = 'Government';
          $icons['historic'] = 'Historic';
          $icons['jewelry'] = 'Jewelry';
          $icons['library'] = 'Library';
          $icons['lodging'] = 'Lodging';
          $icons['monument'] = 'Monument';
          $icons['mountain'] = 'Mountain';
          $icons['movies'] = 'Movie';
          $icons['museum'] = 'Museum';
          $icons['pet'] = 'Pet';
          $icons['police'] = 'Police';
          $icons['post_office'] = 'Post Office';
          $icons['repair'] = 'Repair';
          $icons['restaurant'] = 'Restaurant';
          $icons['school'] = 'School';
          $icons['shopping'] = 'Shopping';
          $icons['ski'] = 'Ski';
          $icons['stadium'] = 'Stadium';
          $icons['supermarket'] = 'Supermarket';
          $icons['taxi'] = 'Taxi';
          $icons['tennis'] = 'Tennis';
          $icons['train'] = 'Train';
          $icons['travel_agent'] = 'Travel Agent';
          $icons['truck'] = 'Truck';
          $icons['university'] = 'University';
          $icons['wine'] = 'Wine';
          $icons['worship_christian'] = 'Worship Christian';
          $icons['worship_general'] = 'Worship General';
          $icons['worship_hindu'] = 'Worship Hindu';
          $icons['worship_islam'] = 'Worship Islam';
          $icons['worship_jewish'] = 'Worship Jewish';
          $icons['zoo'] = 'Zoo';
      }

      return (isset($icons[$slug])) ? $icons[$slug] : '';
    }
}