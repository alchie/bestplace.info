<?php

function get_random_point($conn) {
	$sql = "SELECT place_id,geo_loc_lat,geo_loc_lng,vicinity FROM `places_data` WHERE searched=0 ORDER BY id DESC LIMIT 1";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$rows = array();
	    $row = $result->fetch_assoc();
	    return $row;
	} else {
	    return array(
	    	'place_id' => false,
	    	'geo_loc_lat' => 40.7537636,
	    	'geo_loc_lng'=> -74.0657179,
	    	'vicinity'=>false
	    );
	}
}


function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function slug_exists($slug, $conn) {
	$sql = "SELECT slug FROM `places_data` WHERE slug = '".$slug."';";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		return true;
	} else {
	    return false;
	}
}

function generate_slug($text, $conn) {
	$slug = slugify($text);
	$isExists = slug_exists($slug, $conn);
	if( $isExists ) {
		$n = 1;
		while($isExists) {
			$newslug = $slug."-".$n;
			$isExists = slug_exists($newslug, $conn);
			$n++;
		}
		return $newslug;
	} else {
		return $slug;
	}
}

function add_place($result, $conn) {
	$sql = sprintf("INSERT INTO `places_data` (`place_id`, `name`, `slug`, `icon`, `rating`, `url`, `vicinity`, `website`, `international_phone_number`, `formatted_phone_number`, `formatted_address`, `geo_loc_lat`, `geo_loc_lng`, `lastmod`, `owner_id`) VALUES ('%s', '%s', '%s', NULL, '%s', NULL, '%s', NULL, NULL, NULL, NULL, '%s', '%s', CURRENT_TIMESTAMP, NULL);", $result['place_id'], addslashes($result['name']), generate_slug($result['name'], $conn), $result['rating'], addslashes($result['vicinity']), $result['geo_loc_lat'], $result['geo_loc_lng']);
	//echo $sql . "\n";
	if ($conn->query($sql) === TRUE) {
		echo $result['name']. " - " . $result['vicinity'] . "\n";
	}
}

function add_place_type($place_id, $type, $conn) {
	if( place_type_not_exists($place_id, $type, $conn) ) {
		$sql = sprintf("INSERT INTO `places_type` (`place_id`, `type`) VALUES ('%s', '%s');", $place_id, $type);
		$conn->query($sql);
	}
}

function add_place_photo($place_id, $data, $conn) {
	
	preg_match_all("/<a href=\"(.*)\">(.*)<\/a>/U", $data['html_attributions'][0], $matches);
	$contrib_url = (isset($matches[1][0])) ? $matches[1][0] : '';
	$contributor = (isset($matches[2][0])) ? $matches[2][0] : '';

	$contrib_url = str_replace("https://maps.google.com/maps/contrib/","", $contrib_url);
	$contrib_url = str_replace("/photos","", $contrib_url);
	
	$sql = sprintf("INSERT INTO `places_photos` SET `place_id` = '%s', `photo_reference`= '%s', `height`= '%s', `width`= '%s', `contributor`= '%s', `contrib_url`= '%s';", $place_id, $data['photo_reference'], $data['height'], $data['width'], $contributor, $contrib_url);

	$conn->query($sql);
}

function add_place_review($place_id, $data, $conn) {
	
	$author_id = str_replace("https://www.google.com/maps/contrib/","", $data['author_url']);
	$author_id = str_replace("/reviews","", $author_id);
	
	$sql = sprintf("INSERT INTO `places_reviews` SET `place_id` = '%s', `author_name` = '%s', `author_id` = '%s',	`profile_photo_url` = '%s', `rating` = '%s', `text` = '%s', `time` = '%s';", $place_id, $data['author_name'], $author_id, $data['profile_photo_url'], $data['rating'], $data['text'], $data['time']);

	$conn->query($sql);
}

function place_type_not_exists($place_id, $type, $conn) {
	$sql = sprintf("SELECT * FROM `places_type` WHERE `place_id` = '%s' AND `type` = '%s';", $place_id, $type);
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		return false;
	} else {
	    return true;
	}
}

function fetch_results($results, $conn) {

	if( $results['results'] ) {
		foreach($results['results'] as $res) {
			if( isset($res['place_id']) ) {
				$result = array();
				$result['place_id'] = $res['place_id'];
				$result['rating'] = (isset($res['rating'])) ? $res['rating'] : 0;
				$result['name'] = (isset($res['name'])) ? $res['name'] : '';
				$result['vicinity'] = (isset($res['vicinity'])) ? $res['vicinity'] : '';
				if( isset($res['geometry']) ) {
					$geometry = $res['geometry'];
					if( isset($geometry['location']) ) {
						$location = $geometry['location'];
						$result['geo_loc_lat'] = (isset($location['lat'])) ? $location['lat'] : 0;
						$result['geo_loc_lng'] = (isset($location['lng'])) ? $location['lng'] : 0;
					}
				}
				add_place( $result, $conn );
				foreach( $res['types'] as $type) {
					add_place_type($res['place_id'], $type, $conn);
				}
			}
		}
	}

}

function get_unfinished_data($conn) {
	$sql = "SELECT * FROM `places_data` WHERE done=0 ORDER BY RAND() LIMIT 1";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	    $row = $result->fetch_assoc();
	    return $row;
	} else {
	    return false;
	}
}

function done_with_this_place($place_id, $conn) {
	$sql = "UPDATE places_data SET done = '1' WHERE place_id = '{$place_id}';";
	$conn->query($sql);
}

function update_place_data($place_id, $conn, $key, $value='') {
	$sql = "UPDATE places_data SET {$key} = '{$value}' WHERE place_id = '{$place_id}';";
	$conn->query($sql);
}

function in_address_components($needle, $haystack) {
	foreach( $haystack as $address ) {
		if( in_array($needle, $address['types']) ) {
			return array('long_name'=>$address['long_name'], 'short_name'=>$address['short_name']);
		}
	}
}

function update_place_address_components($place_id, $conn, $address_components) {
	foreach( $address_components as $address ) {
		if( in_array('locality', $address['types']) ) {
			$location_id = get_location_id($conn, $address['short_name']);
			if( $location_id ) {
				insert_place_location($place_id, $location_id, $conn);
			}
		}
	}
}

function get_location_id($conn, $name) {
	$sql = sprintf("SELECT (SELECT l.location_id FROM alchieah_trkms_companies.locations l WHERE l.term_id=t.id) as location_id FROM alchieah_trkms_common.terms t WHERE t.name LIKE '%s' LIMIT 1", $name);
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
	    $row = $result->fetch_assoc();
	    return $row['location_id'];
	} else {
	    return false;
	}
}

function insert_place_location($place_id, $location_id, $conn) {
	$sql = sprintf("SELECT * FROM `places_location` WHERE `place_id` = '%s' AND `location_id` = '%s';", $place_id, $location_id);
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		return false;
	} else {
	    $sql = sprintf("INSERT INTO places_location SET `place_id` = '%s', `location_id` = '%s';", $place_id, $location_id);
	    $conn->query($sql);
	}
}

function insert_state($name, $conn) {
	$sql = sprintf("SELECT * FROM `states` WHERE `name` = '%s';", $name);
	$result = $conn->query( $sql );
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
	    return $row['id'];
	} else {
		$slug = slugify($name);
	    $sql = sprintf("INSERT INTO states SET `name` = '%s',  `slug`= '%s';", $name, $slug);
	    $conn->query($sql);
	    return $conn->insert_id;
	}
}

function insert_county($name, $state_id, $conn) {
	$sql = sprintf("SELECT * FROM `counties` WHERE `name` = '%s' AND `state_id` = '%s';", $name, $state_id);
	$result = $conn->query( $sql );
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
	    return $row['id'];
	} else {
		$slug = slugify($name);
	    $sql = sprintf("INSERT INTO `counties` SET `name` = '%s', `slug`= '%s', `state_id` = '%s';", $name, $slug, $state_id);
	    $conn->query($sql);
	    return $conn->insert_id;
	}
}

function insert_city($name, $county_id, $state_id, $conn) {
	$sql = sprintf("SELECT * FROM `cities` WHERE `name` = '%s' AND `state_id` = '%s' AND `county_id` = '%s';", $name, $state_id, $county_id);
	$result = $conn->query( $sql );
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
	    return $row['id'];
	} else {
		$slug = slugify($name);
	    $sql = sprintf("INSERT INTO `cities` SET `name` = '%s', `slug`= '%s', `state_id` = '%s', `county_id` = '%s';", $name, $slug, $state_id, $county_id);
	    $conn->query($sql);
	    return $conn->insert_id;
	}
}