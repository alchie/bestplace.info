<?php

error_reporting(-1);
ini_set('display_errors', 1);

define('BASEPATH', true);
define('ENVIRONMENT', 'development');

require_once 'lib/googleplaces/src/GooglePlaces.php';
require_once 'lib/googleplaces/src/GooglePlacesClient.php';
require_once 'lib/places.php';
//require_once 'config_dev.php';
require_once 'config.php';

$places_db = $db['places'];

$conn = new mysqli($places_db['hostname'], $places_db['username'], $places_db['password'], $places_db['database']);

if( @$_GET['lat'] && @$_GET['lng'] ) {
	$center_point = array(
		'geo_loc_lat' => $_GET['lat'],
		'geo_loc_lng' => $_GET['lng'],
		'place_id' => false,
		'vicinity' => false,
	);
} else {
	$center_point = get_random_point( $conn );	
}

if( $center_point['vicinity'] ) {
	echo "Looking Near: " . $center_point['vicinity'] . "\n\n";
}

$places = new GooglePlaces($google_place_api_key);
$places->location = array($center_point['geo_loc_lat'],$center_point['geo_loc_lng']);
$places->radius = 1000;
$places->types = array('restaurant','bakery', 'bar', 'cafe', 'lodging');
$results = $places->nearbySearch(); 

fetch_results($results, $conn);

$next_page = (isset($results['next_page_token']) && ($results['next_page_token'] != '')) ? true : false;
while($next_page) {
	$places->pagetoken = $results['next_page_token'];
	$results = $places->nearbySearch();
	$next_page = (isset($results['next_page_token']) && ($results['next_page_token'] != '')) ? true : false;
	fetch_results($results, $conn);
}

if( $center_point['place_id'] ) {
	update_place_data($center_point['place_id'], $conn, 'searched', 1);
}

mysqli_close($conn);