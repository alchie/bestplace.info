<?php

error_reporting(-1);
ini_set('display_errors', 1);

define('BASEPATH', true);
define('ENVIRONMENT', 'development');

require_once 'lib/googleplaces/src/GooglePlaces.php';
require_once 'lib/googleplaces/src/GooglePlacesClient.php';
require_once 'lib/places.php';
//require_once 'config_dev.php';
require_once 'config.php';

$places_db = $db['places'];
$common_db = $db['common'];

$places_conn = new mysqli($places_db['hostname'], $places_db['username'], $places_db['password'], $places_db['database']);
$common_conn = new mysqli($common_db['hostname'], $common_db['username'], $common_db['password'], $common_db['database']);

$place_data = get_unfinished_data($places_conn);

if( $place_data ):

$places = new GooglePlaces($google_place_api_key);
$places->placeid = $place_data['place_id'];
$details = $places->details();

$result = $details['result'];

if( isset($details['error_message']) ) {
	echo $details['error_message'];
	exit;
}

$country = in_address_components('country', $result['address_components'] );

if( $country['short_name'] != 'US' ) {
	exit;
}

$state = in_address_components('administrative_area_level_1', $result['address_components'] );
$county = in_address_components('administrative_area_level_2', $result['address_components'] );
$city = in_address_components('locality', $result['address_components'] );

$state_id = insert_state($state['long_name'], $common_conn);
$county_id = insert_county($county['long_name'], $state_id, $common_conn);
$city_id = insert_city($city['long_name'], $county_id, $state_id, $common_conn);

if( $city_id ) {
	update_place_data($place_data['place_id'], $places_conn, 'city_id', $city_id);
}

if( isset($result['formatted_address'] ) ) {
	update_place_data($place_data['place_id'], $places_conn, 'formatted_address', $result['formatted_address']);
}

if( isset($result['formatted_phone_number'] ) ) {
	update_place_data($place_data['place_id'], $places_conn, 'formatted_phone_number', $result['formatted_phone_number']);
}

if( isset($result['international_phone_number'] ) ) {
	update_place_data($place_data['place_id'], $places_conn, 'international_phone_number', $result['international_phone_number']);
}

if( isset($result['rating'] ) ) {
	update_place_data($place_data['place_id'], $places_conn, 'rating', $result['rating']);
}

if( isset($result['website'] ) ) {
	update_place_data($place_data['place_id'], $places_conn, 'website', $result['website']);
}

if( isset($result['url'] ) ) {
	update_place_data($place_data['place_id'], $places_conn, 'url', $result['url']);
}

if( isset($result['icon'] ) ) {
	$icon = str_replace("http://maps.gstatic.com/mapfiles/place_api/icons/", '', $result['icon']);
	$icon = str_replace("https://maps.gstatic.com/mapfiles/place_api/icons/", '', $icon);
	$icon = str_replace("-71.png", '', $icon);
	update_place_data($place_data['place_id'], $places_conn, 'icon', $icon);
}

if( isset($result['photos'] ) ) {
	foreach($result['photos'] as $photo) {
		add_place_photo($place_data['place_id'], $photo, $places_conn);
	}
}

if( isset($result['reviews'] ) ) {
	foreach($result['reviews'] as $review) {
		add_place_review($place_data['place_id'], $review, $places_conn);
	}
}

done_with_this_place($place_data['place_id'], $places_conn);

mysqli_close($places_conn);

echo $place_data['name'] . " https://us.bestplace.info/" . $place_data['slug'] . ".html";

endif;